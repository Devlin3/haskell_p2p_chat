> import UI.NCurses

Used as constants to decide what rows and columns are valid for a window.

> pad :: Integer
> pad = 5
> accPad :: Integer -> Integer
> accPad  i = i - pad * 2

Creates a window to be used for the UI.

> main :: IO ()
> main = runCurses $ do
>   setEcho False
>   w <- defaultWindow
>   ui w "" []

Renders the current window, then checks to see if the user has input any
new characters.  When the enter key is pressed, the message is added to
the list of previously entered text.  To exit the program, the user must
type the message :q.  If there was no input, simply render loop around.

> ui :: Window -> String -> [String] -> Curses ()
> ui w toSend msgs = do
>   draw w toSend msgs
>   ev <- getEvent w (Just 1000)
>   
>   case ev of
>       Nothing         -> ui w toSend msgs
>       Just (EventCharacter '\n') -> if toSend == ":q" then return ()
>                                       else ui w ""  ([toSend]++ msgs)
>       Just (EventCharacter x) -> ui w (toSend ++ [x]) msgs
>       otherwise -> ui w toSend msgs


Draw function updates the UI.  The first two lines are reserved for user input,
and the rest are used to display previous messages.  

> draw w toSend msgs = do
>   updateWindow w $ do
>       (numR, numC) <- windowSize
>       dims <- return (accPad numR, accPad numC)
>       updateLine dims 0 "User Input:"
>       updateLine dims 1 toSend
>       mapM_ (\(y,msg) -> updateLine dims y msg) (zip [2..]  msgs)
>       moveC dims 1 toSend
>   render

Wrapper over the moveCursor function to guarantee a valid coordinate is passed
in.  Also adds padding to better display on the screen.

> moveC (rows, cols) y msg = if y > rows
>   then return ()
>   else moveCursor (pad + y) (min (cols + pad) (pad + (toInteger (length msg))))

Wrapper over the drawSting function to guarantee a valid coordinate is passed
in.  Also adds padding to better display on the screen.

> updateLine (rows, cols) y str = if y > rows 
>   then return ()
>   else do
>       moveCursor (pad + y) pad
>       clearLine
>       drawString (take (fromIntegral cols) str)
