> import qualified Data.ByteString.Char8 as C
> import Network.Simple.TCP
> import Control.Concurrent
> import Control.Exception
> import Control.Concurrent.STM.TChan
> import Control.Monad.STM
> import System.IO
> import Control.Monad.IO.Class 

Encapsulates a TCP connection and messages to be passed back and forth.
This was added because function parameter lists were becoming too long.

> data Connection = Connection
>                     { serIP :: String
>                       ,serPort :: String
>                       ,remIP :: String
>                       ,remPort :: String
>                       ,gChan :: TChan String
>                     } 

Prompts the user to provide the IP address and port of the user they
would like to communicate with.

> main = do
>   putStrLn "Please type an IP address:"
>   ip <- getLine
>   putStrLn "Please type a port(default 4242):"
>   port <- getLine
>   runServer "localHost" "4242" ip port

Created for debug.  runDebug creates a connection where any message
sent will be immediately sent back.  runS1 and runS2 were created to allow
for debug of two unique users on the same IP address.  This way you can
debug without having to use two different machines.

> runDebug = runServer "localhost" "4242" "localhost" "4242"
> runS1 = runServer "localhost" "4242" "localhost" "4241"
> runS2 = runServer "localhost" "4241" "localhost" "4242"
> runS3 = runServer "localhost" "4240" "localhost" "4242"


Creates all the necessary resources for client and server to communicate.
A thread channel is created and server is started in a background thread.
The new process ID is captured and passed to the client.  This is to ensure
stopping the server when the program terminates.

> runServer :: String -> String -> String -> String -> IO ()
> runServer sIP sPort rIP rPort = do 
>   chan <- atomically $ newTChan
>   con <- return (Connection sIP sPort rIP rPort chan)
>   serverID <- forkIO $ serverLogic con
>   clientCon con serverID

Handles the user interface.  Creates an internal loop that uses the toSend
parameter to keep track of the partial message the user has typed so far.
The function first checks for any new messages from the server and displays
them if necessary. When there is a partial message, it is printed again to
the bottom of the terminal 

    Below is an example of the user having a partially line when receiving
    a message from the other user.  The partial message is echoed after
    the other messages are printed
    
    *Main> runS2
    >Hi how are
    127.0.0.1:34866 "Howdy!"
    >Hi how are

Then the function checks if the user has pressed a key.
The function returns the new partial message and a boolean to track if it is
time to send a message to the other user.  Sent messages are highlighted
by lines that start with Sent: and partial messages that are not sent are
highlighted by lines that start with a >.  

    Below is an example of successfully sending messages to the remote user.
    
    *Main> runS2
    >Hi.
    Sent: Hi.
    >Do you have a minute to IM?
    Sent: Do you have a minute to IM?
    >

> clientCon :: Connection -> ThreadId -> IO ()
> clientCon con sid= do 
>   putChar '>'
>   clientLoop "" where
>       clientLoop toSend = do
>           remoteMsgs <- getRemoteMsgs $ gChan con 
>           if remoteMsgs /= []
>               then do
>                   putStrLn ""
>                   mapM_ putStrLn remoteMsgs
>                   mapM_ putChar ('>' : toSend)
>               else return ()
>
>           (newMsg, ready) <- checkForInput toSend
>
>           if ready
>               then do
>                   status <- processInp newMsg con sid
>                   if status 
>                       then do 
>                           putStrLn ("Sent: " ++ newMsg)
>                           putChar '>'
>                           clientLoop ""
>                       else return ()
>               else clientLoop newMsg
>                   

This function is used to read characters from the keyboard.  Because 
messages could be received while the user is typing, the reading from
the keyboard is non-blocking. See ifReadyDo for more details.  The return
value is a tuple containing the partial message and a boolean to signal
if the user has pressed enter (AKA is ready to send the message).  In
general if a letter that is pressed other than back space, just add the
new character to the string parameter and return false.  When the backspace is pressed,
remove the last character of the message and return false.

> checkForInput :: String -> IO (String, Bool)
> checkForInput toSend = do
>   char <- ifReadyDo stdin getChar
>   case char of
>       Nothing -> return (toSend, False)
>       Just '\b' -> case toSend of
>           ""        -> return (toSend, False)
>           otherwise -> return ((reverse . tail . reverse) toSend, False)
>       Just '\n' -> return (toSend, True)
>       Just x -> return (toSend ++ [x], False)

Function processes a string the user has typed on the commandline.  Any message
that is not ":q" will be attempted to send to the remote user.  A boolean is
returned as a signal of if the message was successfully sent to the other user.

    Below is example of trying to send a message to a remote user that is not
    currently online

    *Main> runS2
    >abc
    Unable to connect to remote

    Unbale to send
    *Main> 

> processInp :: String -> Connection -> ThreadId -> IO Bool
> processInp str con sid
>   | str == ":q" = do
>       killThread sid
>       return False
>
>   | otherwise = do
>       sent <- trySend con str 0 30000
>       if sent 
>           then return True
>           else do 
>               putStrLn ""
>               putStrLn "Unable to send"
>               return False


Handles Socket communication. Every time a message is sent, a new socket
is created.  This is to ensure that the remote machine is still listening.
When a message fails to send, there is a thread delay of one millisecond,
before the attempt is made again.  This will continue until a max number
of attempts is passed. The reason to allow for multiple attempts is to allow
the remote user to reconnect if there is some connection issue.

If the message is successfully sent, the function returns True. If a connection
cannot be established, it returns False. The example above shows how this
function is working when the remote user is not connected.

> trySend :: Connection -> String -> Int -> Int -> IO Bool
> trySend con msg count max = do
>   ret <- try $ connect (remIP con) (remPort con) sendMsg
>   case (ret :: Either IOError Bool) of
>       Right ex -> return True
>       Left val -> checkCon con
>
>   where sendMsg (s, addr) = do
>           send s (C.pack msg)
>           return True
>
>         checkCon con  = if count < max
>           then do
>               threadDelay 1000
>               trySend con msg (count + 1) max
>           else do
>               putStrLn "Unable to connect to remote"
>               return False

Messages that are received by the server are stored in thread channel.
This function reads all messages currently stored in the channel.  Each message
is stored in a list of IO actions.

> getRemoteMsgs :: TChan t -> IO [t]
> getRemoteMsgs chan = do 
>   b <- atomically $ isEmptyTChan chan
>   if b
>       then return []
>       else do
>           val <- atomically $ readTChan chan
>           vals <- getRemoteMsgs chan
>           return (val : vals)

Creates a listening server for processing remote messages.  The function will
execute indefinitely, and has to be killed by the client thread. Because the 
thread is just looping to check for received messages, a yield was added
to improve client responsiveness.

> serverLogic :: Control.Monad.IO.Class.MonadIO m => Connection -> m ()
> serverLogic con = do
>   (serve (Host (serIP con)) (serPort con) listener) where
>       listener (s, addr) = do
>           yield
>           msg <- recv s 1024
>           case msg of
>               Nothing -> listener(s, addr)
>               Just m -> do
>                   atomically $ writeTChan (gChan con)  $ show addr ++ " " ++ show m
>                   listener (s, addr)

The below function will check if there is data ready to be processed from a
handel.  This is used when checking for key pressed.  Simply using getchar
would mean that messages sent from a remote machine could only be displayed
after the user pressed a character.  This code was posted on stack overflow
by Ollie Saunders: https://stackoverflow.com/questions/3894792/what-is-a-
simple-way-to-wait-for-and-then-detect-keypresses-in-haskell

> ifReadyDo :: Handle -> IO a -> IO (Maybe a)
> ifReadyDo hnd x = hReady hnd >>= f
>   where f True = x >>= return . Just
>         f _    = return Nothing
